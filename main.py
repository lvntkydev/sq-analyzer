import csv
import re

def analyze_log_file(log_file_path):
    with open(log_file_path, "r") as file:
        lines = file.readlines()

    total_time = 0
    total_queries = 0
    queries = []

    for line in lines:
        if line.startswith("# Query_time:"):
            total_queries += 1
            time_match = re.search(r'Query_time: ([0-9.]+)', line)
            if time_match:
                total_time += float(time_match.group(1))
        elif line.startswith("SELECT"):
            queries.append(line.strip())

    average_time = total_time / total_queries
    call_time = total_time

    # Writing analysis to a CSV file
    with open("analysis.csv", mode="w", newline="") as csvfile:
        writer = csv.writer(csvfile)
        fields = ["Total Time", "Total Queries", "Average Time", "Call Time", "Query"]
        writer.writerow(fields)

        writer.writerow([total_time, total_queries, average_time, call_time, "\n".join(queries)])

if __name__ == '__main__':
    log_file_path = input("Log file directory: ")
    analyze_log_file(log_file_path)
